import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;
import javax.net.ssl.SSLSocket;
import javax.net.ssl.SSLServerSocket;

public class ClientThread extends Thread{
	//Declarations
	private SSLSocket clientSocket;
	private SSLServerSocket serverSocket;
    private BufferedReader in;
    private PrintWriter out;
	
    //Constructor
	public ClientThread(SSLServerSocket serverSocket){
		this.serverSocket = serverSocket;
	}
	
	@Override
	public void run(){
		try{
			while(true){
                //Initiate Connection and handshake
				clientSocket = (SSLSocket) serverSocket.accept();
				System.out.println("Client Connected.");
				in = new BufferedReader(new InputStreamReader( clientSocket.getInputStream()));
				out = new PrintWriter(clientSocket.getOutputStream(), true);
                String inputLine;
                //Login and Verification
                if(!login()){
                    System.out.println("Client Auth Failed");
                    closeConnection();
                    continue;
                }
				
				//Old school echo loop
				while((inputLine = in.readLine()) != null){
					System.out.println("Client Sent: " + inputLine);
					if(inputLine.equals("Bye.")) 
						break;
				}
                //Close Connection
                closeConnection();
			}
		}catch (IOException io){
			io.printStackTrace();
			System.out.println("Critical Error");
		}		
	}
	
    //Doesn't actually work
	public boolean active(){
		try{
			return clientSocket.isConnected();
		}catch(Exception e){
			return false;
		}
	}
    
    private boolean login() throws IOException{
        //Recieve username
        out.println("Welcome. To login, please enter your username");
        String username = in.readLine();
        //Recieve password
        out.println("Please enter the password");
        String password = in.readLine();
        //Verify Login
        System.out.println("Debug: " + username + ":" + password);
        if(loginManager.login(username, password)){
            out.println("Login Successful");
            return true;
        }else{
            out.println("Login Failed");
            return false;
        }
    }
    
    private void closeConnection() throws IOException{
        System.out.println("Connection Terminated!");
        out.close();
        in.close();
        clientSocket.close();
    }
}