import java.io.*;
import javax.net.ssl.SSLSocket;
import javax.net.ssl.SSLSocketFactory;

public class Client {
    private static PrintWriter out;
    private static BufferedReader in;
    private static SSLSocket echoSocket;
    private static SSLSocketFactory socketFactory;
    private static BufferedReader stdIn;
    
	public static void main(String[] args) throws IOException{
        //Set System parameters
        System.setProperty("javax.net.ssl.trustStore", "./mySrvKeystore");
        System.setProperty("javax.net.ssl.trustStorePassword", "123456");
        
        //Define Hostname, need to set this up based on a user prompt
		String serverHostname = new String ("127.0.0.1");
		
        //Initalise conncetion and handshake
		socketFactory = (SSLSocketFactory) SSLSocketFactory.getDefault();
		echoSocket = (SSLSocket) socketFactory.createSocket(serverHostname, 10007);
		out = new PrintWriter(echoSocket.getOutputStream(), true);
		in = new BufferedReader(new InputStreamReader(echoSocket.getInputStream()));
        //Reader for user input
        stdIn = new BufferedReader(new InputStreamReader(System.in));
        String userInput;
        //Login code and verification
        
		if(!login()){
            System.out.println("Login Failed!\nPlease relaunch client to try again.");
            //Thread.sleep(1000);
            closeConnection();
            System.exit(0);
        }
		
        //Oldschool echo loop
		System.out.print(">> ");
		while ((userInput = stdIn.readLine()) != null){
			out.println(userInput);
			System.out.print(">> ");
			if(userInput.equals("Bye.")) 
				break;
		}
        //Close Connection
        closeConnection();
	}
    
    private static boolean login() throws IOException{
        //Input Username
        System.out.println(in.readLine());
        String username = stdIn.readLine();
        out.println(username);
        //Input password
        System.out.println(in.readLine());
        String password = stdIn.readLine();
        out.println(password);
        //Get response
        if(in.readLine().compareTo("Login Successful") == 0){
            System.out.println("Login Successful");
            return true;
        }else{
            return false;
        }
    }
    
    private static void closeConnection() throws IOException{
		in.close();
		out.close();
		stdIn.close();
		echoSocket.close();
    }
}