import java.io.BufferedReader;
import java.io.FileReader;
import java.io.PrintWriter;
import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.security.MessageDigest;
import javax.crypto.SecretKeyFactory;
import java.math.BigInteger;

public class loginManager{
    public static boolean login(String username, String password){
        try{
            BufferedReader reader = new BufferedReader(new FileReader("users.txt"));
            String compare = username + ":" + hash(password);
            String input = null;
            while((input = reader.readLine()) != null){
                if(compare.compareTo(input) == 0){
                reader.close();
                    return true;
                }
            }
            reader.close();
            return false;
        }catch(IOException e){
            e.printStackTrace();
            return false;
        }
    }
    
    public static boolean addUser(String username, String password){
        try{
            PrintWriter out = new PrintWriter(new BufferedWriter(new FileWriter("users.txt", true)));
            out.println(username + ":" + hash(password));
            out.close();
            return true;
        }catch(IOException e){
            e.printStackTrace();
            return false;
        }
    }
    
    //http://viralpatel.net/blogs/java-md5-hashing-salting-password/
    //Thanks for the humanreadable hashing function
    private static String hash(String input) {        
        String hash = null;
        if(null == input) return null;         
        try {
        MessageDigest digest = MessageDigest.getInstance("SHA-1");         
        digest.update(input.getBytes(), 0, input.length());
        hash = new BigInteger(1, digest.digest()).toString(16);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return hash;
    }
}