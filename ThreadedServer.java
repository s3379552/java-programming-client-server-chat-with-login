import java.io.IOException;
import java.net.ServerSocket;
import java.util.ArrayList;
import javax.net.ssl.SSLServerSocket;
import javax.net.ssl.SSLServerSocketFactory;

public class ThreadedServer {
	public static void main(String args[]) throws IOException{
        //Set System parameters
		System.setProperty("javax.net.ssl.keyStore", "./mySrvKeystore");
        System.setProperty("javax.net.ssl.keyStorePassword", "123456");

		SSLServerSocketFactory serverSocketFactory = (SSLServerSocketFactory) SSLServerSocketFactory.getDefault();
		SSLServerSocket serverSocket = (SSLServerSocket) serverSocketFactory.createServerSocket(10007);
		//Implement is Username/password isn't a thing they let us use.
		//serverSocket.setNeedClientAuth(true);
		
        //Initialise Threadpool
		ArrayList<ClientThread> clientThreads = new ArrayList<ClientThread>();
		int poolSize = 10;
		for(int i = 0; i < poolSize; i++){
			clientThreads.add(new ClientThread(serverSocket));
		}
		for(ClientThread c: clientThreads){
			c.start();
		}
		//Start message
		System.out.println("Server Started. Awaiting Connections...");
	}
}